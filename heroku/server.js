const path = require('path');

const express = require('express');
const app = express();
// Run the app by serving the static files
// in the dist directory
app.use('/assets/config/app.production.json', express.static(path.join(process.cwd(), 'dist/registrar/assets/config/app.production.json'), {
  lastModified: false,
  cacheControl: false,
  etag: false
}));
app.use(express.static(path.join(process.cwd(), '/dist/registrar/')));
// Start the app by listening on the default
// Heroku port
app.listen(process.env.PORT || 8080);

// For all GET requests, send back index.html
// so that PathLocationStrategy can be used
app.get('/*', function(req, res) {
  res.sendFile(path.join(process.cwd() + '/dist/registrar/index.html'));
});
