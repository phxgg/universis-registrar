import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { AppEventService, ServerEvent, ServerEventSubscriber, ToastService } from '@universis/common';

@Injectable()
export class ExamMessageSubscriber implements ServerEventSubscriber {
    constructor(private context: AngularDataContext,
                private toastService: ToastService,
                private translateService: TranslateService,
                private appEvent: AppEventService,
                private router: Router) {
        //
    }
    subscribe(event: ServerEvent): void {
        if (event == null) {
            return;
        }
        if (event.entityType === 'ExamDocumentUploadAction') {
            if (event.target && event.target.id) {
                this.context.model('ExamDocumentUploadActions')
                    .where('id').equal(event.target.id)
                    .expand('actionResult', 'object')
                    .select('id', 'actionResult', 'object')
                    .getItem().then(uploadAction => {
                        if (uploadAction.actionResult && uploadAction.actionResult.additionalType) {
                            const urlTree = this.router.createUrlTree(
                                ['/exams', uploadAction.object.id, 'dashboard', 'grading', uploadAction.id]
                            );
                            // tslint:disable-next-line:max-line-length
                            const title = this.translateService.instant(`Exams.${uploadAction.actionResult.additionalType}.CompletionToast.Title`, uploadAction.object);
                            const message = `
                                <p> ${this.translateService.instant(`Exams.${uploadAction.actionResult.additionalType}.CompletionToast.Message`)} </p>
                                <div>
                                    <a href="#${urlTree.toString()}">${this.translateService.instant(`Exams.${uploadAction.actionResult.additionalType}.CompletionToast.LinkTitle`)}</a>
                                </div>
                            `;
                            // fire app event in case the use is already in grade submission page
                            this.appEvent.change.next({
                                model: 'ExamDocumentUploadActions',
                                target: {
                                    id: uploadAction.id
                                }
                            });
                            // and show toast
                            return this.toastService.show(title, message, true, 30000);
                        }
                    });
            }
        }
    }
}
