import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsDocumentsHomeComponent } from './requests-documents-home.component';

describe('RequestsDocumentsHomeComponent', () => {
  let component: RequestsDocumentsHomeComponent;
  let fixture: ComponentFixture<RequestsDocumentsHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestsDocumentsHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsDocumentsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
