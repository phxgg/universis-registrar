import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassesOverviewStatsOtherRegistrationsComponent } from './classes-overview-stats-other-registrations.component';

describe('ClassesOverviewStatsOtherRegistrationsComponent', () => {
  let component: ClassesOverviewStatsOtherRegistrationsComponent;
  let fixture: ComponentFixture<ClassesOverviewStatsOtherRegistrationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassesOverviewStatsOtherRegistrationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassesOverviewStatsOtherRegistrationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
