import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-classes-courses',
  templateUrl: './classes-courses.component.html',
  styleUrls: []
})
export class ClassesCoursesComponent implements OnInit, OnDestroy  {

  @Input() model: any;
  public classCourse: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.classCourse = await this._context.model('CourseClasses')
        .where('id').equal(params.id)
        .expand('course')
        .getItem();

      this.model = await this._context.model('Courses')
        .where('id').equal(this.classCourse.course.id)
        .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
