import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClassesHomeComponent} from './components/classes-home/classes-home.component';
import {ClassesTableComponent} from './components/classes-table/classes-table.component';
import {ClassesRootComponent} from './components/classes-root/classes-root.component';
import {ClassesStudentsComponent} from './components/classes-dashboard/classes-students/classes-students.component';
import {ClassesInstructorsComponent} from './components/classes-dashboard/classes-instructors/classes-instructors.component';
import { AdvancedFormItemWithLocalesResolver, AdvancedFormRouterComponent } from '@universis/forms';
import {
  AdvancedFormItemResolver,
  AdvancedFormModalComponent,
  AdvancedFormModalData,
  AdvancedFormParentItemResolver
} from '@universis/forms';
import {ClassesAddInstructorComponent} from './components/classes-dashboard/classes-instructors/classes-add-instructor.component';
import {ClassesAddStudentComponent} from './components/classes-dashboard/classes-students/classes-add-student.component';
import { ClassesDashboardComponent } from './components/classes-dashboard/classes-dashboard.component';
import { ClassesOverviewComponent } from './components/classes-dashboard/classes-overview/classes-overview.component';
import { ClassesExamsComponent } from './components/classes-dashboard/classes-exams/classes-exams.component';
import {ClassesSectionsComponent} from './components/classes-dashboard/classes-sections/classes-sections.component';
import {
  ActiveDepartmentIDResolver,
  ActiveDepartmentResolver, CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver, LastStudyProgramResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import {CourseTitleResolver} from '../courses/components/dashboard/courses-preview-classes/courses-preview-classes.component';
import {SelectReportComponent} from '../reports-shared/components/select-report/select-report.component';
import {ItemRulesModalComponent, RuleFormModalData} from '../rules';
import { ClassesBooksComponent } from './components/classes-dashboard/classes-books/classes-books.component';
import { SearchConfigurationResolver, TableConfigurationResolver } from '../registrar-shared/table-configuration.resolvers';

const routes: Routes = [
    {
        path: '',
        component: ClassesHomeComponent,
        data: {
            title: 'Classes'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list/current'
            },
          {
            path: 'list',
            pathMatch: 'full',
            redirectTo: 'list/index'
          },
            {
                path: 'list/:list',
                component: ClassesTableComponent,
                data: {
                    model: 'CourseClasses',
                    title: 'Classes List'
                },
                resolve: {
                  currentYear: CurrentAcademicYearResolver,
                  currentPeriod: CurrentAcademicPeriodResolver,
                  tableConfiguration: TableConfigurationResolver,
                  searchConfiguration: SearchConfigurationResolver
                },
              children: [
                {
                  path: 'item/:id/rules',
                  pathMatch: 'full',
                  component: ItemRulesModalComponent,
                  outlet: 'modal',
                  data: <RuleFormModalData> {
                    model: 'CourseClasses',
                    closeOnSubmit: true,
                    serviceQueryParams: {
                      $expand: 'course',
                    },
                    navigationProperty: 'RegistrationRules'
                  },
                  resolve: {
                    department: ActiveDepartmentIDResolver,
                    data: AdvancedFormItemResolver
                  }
                },
              ]
            }
        ]
    },
  {
    path: 'create',
    component: ClassesRootComponent,
    children: [
      {
        path: 'new',
        pathMatch: 'full',
        component: AdvancedFormRouterComponent,
        resolve: {
          department: ActiveDepartmentResolver
        }
      }
    ]
  },
    {
        path: ':id',
        component: ClassesRootComponent,
        data: {
            title: 'Classes Home'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'dashboard'
          },
          {
            path: 'dashboard',
            component: ClassesDashboardComponent,
            data: {
              title: 'Classes Dashboard'
            },
            children: [
              {
                path: '',
                pathMatch: 'full',
                redirectTo: 'overview'
              },
              {
                path: 'overview',
                component: ClassesOverviewComponent,
                data: {
                  title: 'Classes.Overview'
                },
                children: [
                  {
                    path: 'print',
                    pathMatch: 'full',
                    component: SelectReportComponent,
                    outlet: 'modal',
                    resolve: {
                      item: AdvancedFormItemResolver
                    }
                  }
              ]
              },
              {
                path: 'students',
                component: ClassesStudentsComponent,
                data: {
                  title: 'Classes.Students',
                  model : 'StudentCourseClasses'
                },
                resolve: {
                  searchConfiguration: SearchConfigurationResolver,
                  tableConfiguration: TableConfigurationResolver
                },
                children: [
                  {
                    path: 'add',
                    pathMatch: 'full',
                    component: ClassesAddStudentComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      title: 'Classes.AddStudent'
                    },
                    resolve: {
                      courseClass: AdvancedFormItemResolver
                    }
                  }
                ]
              },
              {
                path: 'instructors',
                component: ClassesInstructorsComponent,
                data: {
                  title: 'Classes.Instructors'
                },
                children: [
                  {
                    path: 'add',
                    pathMatch: 'full',
                    component: ClassesAddInstructorComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      title: 'Classes.AddInstructor'
                    },
                    resolve: {
                      courseClass: AdvancedFormItemResolver
                    }
                  },
                  {
                    path: ':id/edit',
                    pathMatch: 'full',
                    component: AdvancedFormModalComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      model: 'CourseClassInstructors',
                      action: 'edit',
                      closeOnSubmit: true,
                      serviceQueryParams: {
                        $expand: 'role,instructor($expand=department)',
                      }
                    },
                    resolve: {
                      data: AdvancedFormItemResolver,
                    }
                  }
                ]
              },
              {
                path: 'exams',
                component: ClassesExamsComponent,
                data: {
                  title: 'Classes.Exams',
                  model: 'CourseExamClasses'
                },
                resolve: {
                  tableConfiguration: TableConfigurationResolver
                }
              },
              {
                path: 'sections',
                component: ClassesSectionsComponent,
                data: {
                  title: 'Classes.Sections'
                },
                children: [
                  {
                    path: 'add',
                    pathMatch: 'full',
                    component: AdvancedFormModalComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      model: 'CourseClassSections',
                      closeOnSubmit: true,
                      action: 'new',
                      serviceQueryParams: {
                        $expand: 'course',
                      },
                    },
                    resolve: {
                      courseClass: AdvancedFormParentItemResolver,
                      department: ActiveDepartmentResolver,
                      year: CurrentAcademicYearResolver,
                      period: CurrentAcademicPeriodResolver
                    }
                  },
                  {
                    path: ':id/edit',
                    pathMatch: 'full',
                    component: AdvancedFormModalComponent,
                    outlet: 'modal',
                    data: <AdvancedFormModalData> {
                      model: 'CourseClassSections',
                      action: 'edit',
                      closeOnSubmit: true,
                      serviceQueryParams: {
                        $expand: 'courseClass($expand=course,statistic)',
                      }
                    },
                    resolve: {
                      data: AdvancedFormItemResolver
                    }
                  },
                  {
                    path: 'item/:id/rules',
                    pathMatch: 'full',
                    component: ItemRulesModalComponent,
                    outlet: 'modal',
                    data: <RuleFormModalData> {
                      model: 'CourseClassSections',
                      closeOnSubmit: true,
                      serviceQueryParams: {
                      },
                      navigationProperty: 'CourseClassSectionRules'
                    },
                    resolve: {
                      department: ActiveDepartmentIDResolver,
                      data: AdvancedFormItemResolver
                    }
                  },
                ]
              },
              {
                path: 'books',
                component: ClassesBooksComponent,
                data: {
                  title: 'Classes.AcademicTextbooks.Books',
                  model: 'Books',
                },
                resolve: {
                  tableConfiguration: TableConfigurationResolver
                }
              }
            ]
          },
          {
            path: 'shared',
            component: ClassesStudentsComponent,
            data: {
              title: 'Classes.Students',
              model: 'StudentCourseClasses'
            },
            resolve: {
              searchConfiguration: SearchConfigurationResolver,
              tableConfiguration: TableConfigurationResolver
            },
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: ClassesAddStudentComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  title: 'Classes.AddStudent'
                },
                resolve: {
                  courseClass: AdvancedFormItemResolver
                }
              }
            ]
          },
          {
            path: ':action',
            component: AdvancedFormRouterComponent,
            data: {
              serviceQueryParams: {
                $expand: 'period, status, year, statistic, locales, course($expand=department)'
              }
            },
            resolve: {
              data: AdvancedFormItemWithLocalesResolver
            }
          }
        ]
      }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class ClassesRoutingModule {
}
