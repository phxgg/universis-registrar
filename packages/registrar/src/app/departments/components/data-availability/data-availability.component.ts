import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { AngularDataContext } from "@themost/angular";
import { DiagnosticsService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService } from "@universis/common";
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult, AdvancedTableEditorDirective } from "@universis/ngx-tables";
import { DATA_AVAILABILITY_CONFIG } from "./data-availability.list";
import { ExportSpreadsheetService } from "@universis/common";

@Component({
    selector: 'app-department-data-availability',
    templateUrl: './data-availability.component.html',
    styles: [
        `
        .dropdown-item.disabled, .dropdown-item:disabled {
            color: var(--gray-200) !important;
        }
        `
    ]
})
export class DepartmentDataAvailability implements AfterViewInit, OnInit, OnDestroy {

    public recordsTotal: number;
    public readonly tableConfiguration = AdvancedTableConfiguration.cast(DATA_AVAILABILITY_CONFIG);
    @ViewChild('table') table: AdvancedTableComponent;
    @ViewChild(AdvancedTableEditorDirective) tableEditor: AdvancedTableEditorDirective;
    private fragmentSubscription: any;
    supported: boolean = true;

    constructor(private context: AngularDataContext,
        private errorService: ErrorService,
        private loadingService: LoadingService,
        private activatedRoute: ActivatedRoute,
        private modalService: ModalService,
        private translateService: TranslateService,
        private diagnosticsService: DiagnosticsService,
        private exportSpreadsheet: ExportSpreadsheetService) {
    }
    ngOnInit(): void {
        this.diagnosticsService.getServices().then((services) => {
            this.supported = services.findIndex((service) => service.serviceType === 'DiplomaInspectorService') >= 0;
            // fetch data
            if (this.supported) {
                this.fetch();
            }
        });
    }

    ngAfterViewInit(): void {
        this.fragmentSubscription = this.activatedRoute.fragment.subscribe((fragment) => {
            if (fragment === 'reload') {
                this.fetch();
            }
        });
    }

    fetch() {
        if (!this.supported) {
            return;
        }
        this.context.model('LocalDepartments').select(
            'id',
            'name',
            'alternativeCode'
        ).expand('dataAvailability($expand=createdBy,modifiedBy)').take(-1).getItems().then((results) => {
            const items = results.map((item: { id: any, name: string, alternativeCode: string, dataAvailability?: any }) => {
                return {
                    id: item.id,
                    alternativeCode: item.alternativeCode,
                    name: item.name,
                    dataAvailability: item.dataAvailability && item.dataAvailability.id,
                    availableSince: item.dataAvailability && item.dataAvailability.availableSince,
                    availableSinceDescription: item.dataAvailability && item.dataAvailability.availableSinceDescription,
                    dateCreated: item.dataAvailability && item.dataAvailability.dateCreated,
                    createdBy: item.dataAvailability && item.dataAvailability.createdBy && item.dataAvailability.createdBy.name,
                    dateModified: item.dataAvailability && item.dataAvailability.dateModified,
                    modifiedBy: item.dataAvailability &&  item.dataAvailability.modifiedBy && item.dataAvailability.modifiedBy.name
                }
            });
            this.table.selectNone();
            this.tableEditor.set(items);
        }).catch((err) => {
            console.error(err);
            this.errorService.showError(err, {
                continueLink: '.'
            });
        });
    }

    ngOnDestroy(): void {

    }

    remove() {
        const RemoveDialog = this.translateService.instant('DataAvailabilityAttributes.Remove');
        this.modalService.showWarningDialog(RemoveDialog.Title, RemoveDialog.Message, DIALOG_BUTTONS.YesNo).then((value) => {
            if (value === 'no') {
                return;
            }
            this.loadingService.showLoading();
            const items = this.table.selected.filter((item) => item.dataAvailability != null).map((item) => {
                return {
                    id: item.id,
                    dataAvailability: null
                }
            });
            void this.context.model('LocalDepartments').save(items).then(() => {
                this.loadingService.hideLoading();
                this.fetch();
            }).catch((err) => {
                this.loadingService.hideLoading();
                this.errorService.showError(err, {
                    continueLink: '.'
                });
            });
        });
    }

    autoUpdate() {
        const UpdateDialog = this.translateService.instant('DataAvailabilityAttributes.Update');
        this.modalService.showSuccessDialog(UpdateDialog.Title, UpdateDialog.Message, DIALOG_BUTTONS.YesNo).then((value) => {
            if (value === 'no') {
                return;
            }
            const selected = this.table.selected;
            this.loadingService.showLoading();
            this.context.model('Students')
                .where('studentStatus/alternateName').equal('graduated')
                .and('graduationDate').notEqual(null)
                .select('department', 'min(graduationDate) as availableSince')
                .groupBy('department')
                .take(-1)
                .getItems().then((results: any[]) => {
                    const items = results.map((item: { department: any, availableSince: Date | null }) => {
                        return {
                            id: item.department,
                            dataAvailability: {
                                availableSince: item.availableSince
                            }
                        }
                    }).filter((item) => {
                        return selected.findIndex((x) => x.id === item.id) >= 0;
                    });
                    return this.context.model('LocalDepartments').save(items).then(() => {
                        this.loadingService.hideLoading();
                        this.fetch();
                    })
                }).catch((err) => {
                    this.loadingService.hideLoading();
                    this.errorService.showError(err, {
                        continueLink: '.'
                    });
                });
        });
    }

    /**
   * Data load event handler of advanced table component
   * @param {AdvancedTableDataResult} data
   */
    onDataLoad(data: AdvancedTableDataResult) {
        this.recordsTotal = data.recordsTotal;
    }

    export() {
        const rows = this.table.dataTable.rows({ search: 'applied' }).data().toArray() as any[];
        if (rows.length === 0) {
            // nothing to do
            return;
        }
        // get first row
        // get column names
        const header: any[] = Object.keys(rows[0]).map((key) => {
            const column = this.table.config.columns.find((column) => {
                const name = column.property || column.name;
                return name === key;
            });
            if (column && column.title) {
                return this.translateService.instant(column.title);
            }
            return this.translateService.instant(key);
        });
        const file = `${this.translateService.instant('DataAvailabilityAttributes.Title')}.xlsx`
        this.exportSpreadsheet.export(rows, file, {
            header,
            skipHeader: false
        });
    }

}
