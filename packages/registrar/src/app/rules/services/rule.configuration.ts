import {InjectionToken} from '@angular/core';

export interface RuleConfiguration {
  types: DataModelRule[];
}

export interface DataModelRule {
  entitySet: string;
  navigationProperty: string;
  children?: string[];
}

export const RULES_CONFIG = new InjectionToken<RuleConfiguration>('rules.config');
// tslint:disable quotemark
export const DEFAULT_RULES_CONFIG: RuleConfiguration = {
  types: [
    {
      'entitySet': 'CourseClasses',
      'navigationProperty': 'RegistrationRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'RegisteredCourseRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'CourseCategoryRule',
        'ThesisRule',
        'InternshipRule'
      ]
    },
    {
      'entitySet': 'CourseClasses',
      'navigationProperty': 'SectionRegistrationRules',
      'children': [
        'StudentRule'
      ]
    },
    {
      'entitySet': 'StudyProgramSpecialties',
      'navigationProperty': 'GraduationRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'CourseCategoryRule',
        'ThesisRule',
        'InternshipRule',
        'ProgramGroupRule'
      ]
    },
    {
      'entitySet': 'StudyPrograms',
      'navigationProperty': 'InscriptionRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'CourseCategoryRule',
        'ThesisRule',
        'InternshipRule',
        'ProgramGroupRule'
      ]
    },
    {
      'entitySet': 'StudyPrograms',
      'navigationProperty': 'InternshipRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'CourseCategoryRule',
        'ThesisRule',
        'ProgramGroupRule'
      ]
    },
    {
      'entitySet': 'StudyPrograms',
      'navigationProperty': 'ThesisRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'CourseCategoryRule',
        'InternshipRule',
        'ProgramGroupRule'
      ]
    },
    {
      'entitySet': 'StudyPrograms',
      'navigationProperty': 'graduateCalculationMode',
      'children': [
        'CourseTypeCalculationRule',
        'StudentThesisCalculationRule',
        'SemesterCalculationRule'
      ]
    },
    {
      'entitySet': 'StudyProgramSpecialties',
      'navigationProperty': 'SpecialtyRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'CourseCategoryRule',
        'ThesisRule',
        'InternshipRule',
        'ProgramGroupRule'
      ]
    },
    {
      'entitySet': 'ProgramCourses',
      'navigationProperty': 'RegistrationRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseCategoryRule',
        'CourseSectorRule',
        'ThesisRule',
        'InternshipRule',
        'ProgramGroupRule',
        'RegisteredCourseRule'
      ]
    },
    {
      'entitySet': 'Scholarships',
      'navigationProperty': 'ScholarshipRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'CourseCategoryRule',
        'ThesisRule',
        'InternshipRule',
        'YearMeanGradeRule',
        'MeanGradeRule'
      ]
    },
    {
      'entitySet': 'Students',
      'navigationProperty': 'StudentGraduationRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'CourseCategoryRule',
        'ThesisRule',
        'InternshipRule',
        'ProgramGroupRule'
      ]
    },
    {
      'entitySet': 'ProgramGroups',
      'navigationProperty': 'RegistrationRules',
      'children': [
        'StudentRule',
        'CourseRule',
        'CourseTypeRule',
        'CourseAreaRule',
        'CourseSectorRule',
        'CourseCategoryRule',
        'ThesisRule',
        'InternshipRule',
        'ProgramGroupRule'
      ]
    },
    {
      'entitySet': 'CourseClassSections',
      'navigationProperty': 'CourseClassSectionRules',
      'children': [
        'StudentRule'
      ]
    },
    {
      'entitySet': 'DocumentConfigurations',
      'navigationProperty': 'RequestRules',
      'children': [
        'StudentRule',
        'InternshipRule',
        'ThesisRule'
      ]
    },
    {
      'entitySet': 'StudentRequestConfigurations',
      'navigationProperty': 'RequestRules',
      'children': [
        'StudentRule',
        'InternshipRule',
        'ThesisRule'
      ]
    }
  ]
};
// tslint:enable quotemark


