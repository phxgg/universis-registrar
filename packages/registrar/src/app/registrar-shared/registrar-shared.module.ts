import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule, OnInit, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {
  ApplicationSettingsConfiguration,
  ErrorModule,
  SIDEBAR_LOCATIONS,
  GUEST_SIDEBAR_LOCATIONS,
  AppSidebarService,
  AppGuestSidebarService,
  ServerEventModule,
  SharedModule
} from '@universis/common';
import {ActiveDepartmentService} from './services/activeDepartmentService.service';
import {AdvancedFormsModule} from '@universis/forms';
import {RouterModule} from '@angular/router';
import {MostModule} from '@themost/angular';
import {FormioModule} from 'angular-formio';
import * as sidebarLocations from '../app.sidebar.locations';
import * as guestSidebarLocations from '../app.guest-sidebar.locations';
import { ActionsButtonComponent } from './actions-button/actions-button.component';
import { MessageSubscriber } from './services/MessageSubscriber';
import { StatusGuard } from './guards/status.guard';
import { AttachmentDirectDownloadComponent } from './attachment-direct-download/attachment-direct-download.component';
import { ApplicationDatabase } from './services/app-db.service';
import { TablesModule } from '@universis/ngx-tables';
import { TableActionsComponent } from './table-actions.component';
import { SearchConfigurationResolver, TableConfigurationResolver } from './table-configuration.resolvers';
import { AdvancedFormStudentWithLocalesResolver, AdvancedFormStudyProgramWithLocalesResolver } from './resolvers';

export declare interface ApplicationSettings extends ApplicationSettingsConfiguration {
  excludeRequestTypes?: Array<string>;
  useDigitalSignature: boolean;
  title?: string;
  maxExportItems?: number;
}

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      TranslateModule,
      AdvancedFormsModule,
      ErrorModule,
      MostModule,
      FormioModule,
      ServerEventModule,
      TablesModule,
      SharedModule
  ],
  declarations: [
    ActionsButtonComponent,
    AttachmentDirectDownloadComponent,
    TableActionsComponent
  ],
  providers: [
    {
      provide: SIDEBAR_LOCATIONS,
      useValue: sidebarLocations.REGISTRAR_SIDEBAR_LOCATIONS
    },
    {
      provide: GUEST_SIDEBAR_LOCATIONS,
      useValue: guestSidebarLocations.REGISTRAR_GUEST_SIDEBAR_LOCATIONS
    },
    StatusGuard
  ],
  exports: [
    ActionsButtonComponent,
    AttachmentDirectDownloadComponent,
    TableActionsComponent
  ],
  schemas: [
      CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class RegistrarSharedModule implements OnInit {
  constructor(@Optional() @SkipSelf() parentModule: RegistrarSharedModule,
              private _translateService: TranslateService,
              private _sidebarService: AppSidebarService,
              private _guestSidebarService: AppGuestSidebarService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading registrar shared module');
      console.error(err);
    });
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: RegistrarSharedModule,
      providers: [
        ActiveDepartmentService,
        ApplicationDatabase,
        MessageSubscriber,
        StatusGuard,
        TableConfigurationResolver,
        SearchConfigurationResolver,
        AdvancedFormStudentWithLocalesResolver,
        AdvancedFormStudyProgramWithLocalesResolver
      ]
    };
  }

  async ngOnInit() {
    // create promises chain
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`../../assets/i18n/${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);

    this._translateService.onDefaultLangChange.subscribe(() => {
      this._sidebarService.loadConfig();
      this._guestSidebarService.loadConfig();
    });
  }

}
