import {Component, OnInit, ViewChild} from '@angular/core';
import {ConfigurationService, UserService} from '@universis/common';
import {AppGuestSidebarService} from '@universis/common';

@Component({
  selector: 'app-guest-layout',
  templateUrl: './guest-layout.component.html',
  styles: [``]
})
export class GuestLayoutComponent implements OnInit {

  constructor(private _userService: UserService,
              private _configurationService: ConfigurationService,
              private _appSidebarService: AppGuestSidebarService) { }

  @ViewChild('appSidebarNav') appSidebarNav: any;
  public user;
  public today = new Date();
  public languages;
  public currentLang;
  public applicationImage;

  async ngOnInit() {
    // get sidebar navigation items
    this.appSidebarNav.navItems = this._appSidebarService.navigationItems;
    // get current user
    this.user = await this._userService.getUser();
    // get current language
    this.currentLang = this._configurationService.currentLocale;
    // get languages
    this.languages = this._configurationService.settings.i18n.locales;
    // get path of brand logo
    this.applicationImage = this._configurationService.settings.app && this._configurationService.settings.app.image;
  }

  changeLanguage(lang) {
    this._configurationService.currentLocale = lang;
    // reload current route
    window.location.reload();
  }
}
