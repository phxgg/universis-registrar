import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramCoursePreviewComponent } from './program-course-preview.component';

describe('ProgramCoursePreviewComponent', () => {
  let component: ProgramCoursePreviewComponent;
  let fixture: ComponentFixture<ProgramCoursePreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramCoursePreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramCoursePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
