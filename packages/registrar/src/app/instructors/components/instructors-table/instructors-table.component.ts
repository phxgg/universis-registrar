import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AdvancedRowActionComponent, AdvancedTableComponent, AdvancedTableDataResult } from '@universis/ngx-tables';
import { ActivatedRoute, Router } from '@angular/router';
import { ActivatedTableService } from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { Observable, Subscription } from 'rxjs';
import { ConfigurationService, ErrorService, LoadingService, ModalService, UserActivityService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import { ElotConverter } from '@universis/elot-converter';
import { ClientDataQueryable } from '@themost/client';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';

@Component({
  selector: 'app-instructors-table',
  templateUrl: './instructors-table.component.html'
})
export class InstructorsTableComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  public defaultLocale: string;
  private dataSubscription: Subscription;
  private selectedItems = [];
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _loadingService: LoadingService,
    private _modalService: ModalService,
    private _errorService: ErrorService,
    private _context: AngularDataContext,
    private _configurationService: ConfigurationService,
    private _activeDepartmentService: ActiveDepartmentService
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = data.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
      this.defaultLocale = this._configurationService.settings
        && this._configurationService.settings.i18n
        && this._configurationService.settings.i18n.defaultLocale;
      this._userActivityService.setItem({
        category: this._translateService.instant('Instructors.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });

  }

  async convertToElot() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItemsWithLocales();
      const elotMap = new Map<number, {familyName: string, givenName: string, enLocale: any}>();
      // validate familyName, givenName
      this.selectedItems = items.filter(instructor => {
        const convertedFamilyName = ElotConverter.convert(instructor.familyName);
        const convertedGivenName = ElotConverter.convert(instructor.givenName);
        // try to find en locale
        const enLocale = instructor.locales.find(locale => locale.inLanguage === 'en');
        // add to map to save conversion for later
        elotMap.set(instructor.id, {familyName: convertedFamilyName, givenName: convertedGivenName, enLocale: enLocale});
        if (enLocale) {
          return (enLocale.familyName !== convertedFamilyName || enLocale.givenName !== convertedGivenName);
        }
        return instructor;
      });
      this._loadingService.hideLoading();
      // open modal
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Instructors.ConvertToElotAction.Title',
          description: 'Instructors.ConvertToElotAction.Description',
          errorMessage: 'Instructors.ConvertToElotAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeConvertToElotAction(elotMap)
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeConvertToElotAction(elotMap: Map<number, {familyName: string, givenName: string, enLocale: any}>) {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // get converted values from map
            const convertedValues = elotMap.get(item.id);
            // update locales
            if (convertedValues.enLocale) {
              // if locale exists, update it
              Object.assign(convertedValues.enLocale, {
                familyName: convertedValues.familyName,
                givenName: convertedValues.givenName
              });
            } else {
              if (Array.isArray(item.locales)) {
                // create en locale
                item.locales.push({
                  inLanguage: 'en',
                  familyName: convertedValues.familyName,
                  givenName: convertedValues.givenName
                });
              }
            }
            // and save
            await this._context.model('Instructors').save(item);
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async createUser() {
    try {
      this._loadingService.showLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      if (!(activeDepartment && activeDepartment.departmentConfiguration && activeDepartment.departmentConfiguration.instructorUsernameFormat)) {
        this._loadingService.hideLoading();
        // show informative message to the user about the required settings, if they are not set
        return this._modalService.showInfoDialog(this._translateService.instant('Instructors.CreateUserAction.SettingsRequired'),
          this._translateService.instant('Instructors.CreateUserAction.SettingsRequiredHelp'));
      }
      const items = await this.getSelectedItems();
      // get only active students
      this.selectedItems = items.filter(item => item.user == null);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Instructors.CreateUserAction.Title',
          description: 'Instructors.CreateUserAction.Description',
          refresh: this.refreshAction,
          errorMessage: 'Instructors.CreateUserAction.CompletedWithErrors',
          execute: this.executeCreateUser()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async linkUser() {
    try {
      this._loadingService.showLoading();
      this.selectedItems = await this.getSelectedItems();
      if (this.selectedItems.length > 1) {
        this._loadingService.hideLoading();
        // at this moment, link use action can be applied to only one instructor
        return this._modalService.showInfoDialog(this._translateService.instant('Instructors.LinkUserAction.Title'),
          this._translateService.instant('Instructors.LinkUserAction.ForSingleInstructor.Message'));
      }
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Instructors/linkUser' : null,
          modalTitle: 'Instructors.LinkUserAction.Title',
          description: 'Instructors.LinkUserAction.Description',
          refresh: this.refreshAction,
          errorMessage: 'Instructors.LinkUserAction.CompletedWithErrors',
          execute: this.executeLinkUser()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async unlinkUser() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter(item => item.user != null);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Instructors.UnlinkUserAction.Title',
          description: 'Instructors.UnlinkUserAction.Description',
          refresh: this.refreshAction,
          errorMessage: 'Instructors.UnlinkUserAction.CompletedWithErrors',
          execute: this.executeUnlinkUser()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeUnlinkUser() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const unlinkUser = {
              id: item.id,
              user: null,
              $state: 2
            };
            // unlink user
            await this._context.model('Instructors').save(unlinkUser);
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  executeLinkUser() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (!data.user) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            Object.assign(data , {
              id: item.id,
              $state: 2
            });
            await this._context.model(`Instructors`).save(data);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  executeCreateUser() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // create user
            await this._context.model(`Instructors/${item.id}/CreateUser`).save(null);
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'user'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              user: item.user
            };
          });
        }
      }
    }
    return items;
  }

  async getSelectedItemsWithLocales() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'familyName', 'givenName', 'locales'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .expand('locales')
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          const itemPromises = this.table.selected.map(async item => await this._context.model('Instructors')
            .where('id').equal(item.id).select('id, familyName, givenName, locales').expand('locales').getItem());
          items = await Promise.all(itemPromises);
        }
      }
    }
    return items;
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
