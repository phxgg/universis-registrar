import {
  Action,
  ActionReducer,
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { merge } from 'lodash';
import * as fromSettings from '../settings/reducers';
export interface AppState {
  settings: fromSettings.SettingsState;
}

export const reducers: ActionReducerMap<AppState> = {
  settings: fromSettings.filterReducer
};

function sessionStorageMergeMetaReducer<S, A extends Action = Action>(reducer: ActionReducer<S, A>) {
  let onInit = true;
  return function(state: S, action: A): S {
    const nextState = reducer(state, action);
    // init the application state.
    const savedState = JSON.parse(sessionStorage.getItem('registrar.state')) || {};
    if (onInit) {
      onInit  = false;
      return merge(nextState, savedState);
    }
    const setState = merge(savedState, nextState);
    sessionStorage.setItem('registrar.state', JSON.stringify(setState));
    return nextState;
  };
}

function localStorageMergeMetaReducer<S, A extends Action = Action>(reducer: ActionReducer<S, A>) {
  let onInit = true;
  return function(state: S, action: A): S {
    const nextState = reducer(state, action);
    // init the application state.
    const savedState = JSON.parse(localStorage.getItem('registrar.state')) || {};
    if (onInit) {
      onInit  = false;
      return merge(nextState, savedState);
    }
    const setState = merge(savedState, nextState);
    localStorage.setItem('registrar.state', JSON.stringify(setState));
    return nextState;
  };
}

const metaReducers: MetaReducer<AppState>[] = [
  localStorageMergeMetaReducer
];

export {
  sessionStorageMergeMetaReducer,
  localStorageMergeMetaReducer,
  metaReducers
};
