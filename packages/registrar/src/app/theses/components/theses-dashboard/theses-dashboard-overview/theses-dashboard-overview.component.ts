import { Component, OnDestroy, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-theses-dashboard-overview',
  templateUrl: './theses-dashboard-overview.component.html'
})
export class ThesesDashboardOverviewComponent implements OnInit, OnDestroy {

  public model: any;
  private subscription: Subscription;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Theses')
        .where('id').equal(params.id)
        .expand('instructor,type,status,students($expand=student($expand=department,person)),gradeScale')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
