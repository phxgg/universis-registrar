import { TableColumnConfiguration } from '@universis/ngx-tables';

export const STUDENT_COLUMNS_CONFIG: TableColumnConfiguration[] = [
  {
    name: 'person/familyName',
    property: 'personFamilyName',
    title: 'StudentColumns.personFamilyName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/givenName',
    property: 'personGivenName',
    title: 'StudentColumns.personGivenName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/gender/alternateName',
    property: 'personGenderAlternateName',
    title: 'StudentColumns.personGenderAlternateName',
    formatter: 'TranslationFormatter',
    formatString: 'Genders.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'person/fatherName',
    property: 'personFatherName',
    title: 'StudentColumns.personFatherName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/motherName',
    property: 'personMotherName',
    title: 'StudentColumns.personMotherName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/spouseName',
    property: 'personSpouseName',
    title: 'StudentColumns.personSpouseName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/citizenRegistrar',
    property: 'personCitizenRegistrar',
    title: 'StudentColumns.personCitizenRegistrar',
    hidden: true,
    optional: true
  },
  {
    name: 'person/citizenRegistrarPlace',
    property: 'personCitizenRegistrarPlace',
    title: 'StudentColumns.personCitizenRegistrarPlace',
    hidden: true,
    optional: true
  },
  {
    name: 'person/citizenRegistrarRegion/name',
    property: 'personCitizenRegistrarRegionName',
    title: 'StudentColumns.personCitizenRegistrarRegionName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/maleRegistrar',
    property: 'personMaleRegistrar',
    title: 'StudentColumns.personMaleRegistrar',
    hidden: true,
    optional: true
  },
  {
    name: 'person/maleRegistrarPlace',
    property: 'personMaleRegistrarPlace',
    title: 'StudentColumns.personMaleRegistrarPlace',
    hidden: true,
    optional: true
  },
  {
    name: 'person/maleRegistrarRegion/name',
    property: 'personMaleRegistrarRegionName',
    title: 'StudentColumns.personMaleRegistrarRegionName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/identityCard',
    property: 'personIdentityCard',
    title: 'StudentColumns.personIdentityCard',
    hidden: true,
    optional: true
  },
  {
    name: 'person/identityType/name',
    property: 'personIdentityTypeName',
    title: 'StudentColumns.personIdentityTypeName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/identityDate',
    property: 'personIdentityDate',
    title: 'StudentColumns.personIdentityDate',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'person/identityAuthority',
    property: 'personIdentityAuthority',
    title: 'StudentColumns.personIdentityAuthority',
    hidden: true,
    optional: true
  },
  {
    name: 'person/vatNumber',
    property: 'personVatNumber',
    title: 'StudentColumns.personVatNumber',
    hidden: true,
    optional: true
  },
  {
    name: 'person/vatOffice',
    property: 'personVatOffice',
    title: 'StudentColumns.personVatOffice',
    hidden: true,
    optional: true
  },
  {
    name: 'person/birthDate',
    property: 'personBirthDate',
    title: 'StudentColumns.personBirthDate',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'person/birthPlace',
    property: 'personBirthPlace',
    title: 'StudentColumns.personBirthPlace',
    hidden: true,
    optional: true
  },
  {
    name: 'person/birthPlaceRegion/name',
    property: 'personBirthPlaceRegionName',
    title: 'StudentColumns.personBirthPlaceRegionName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/email',
    property: 'personEmail',
    title: 'StudentColumns.personEmail',
    hidden: true,
    optional: true
  },
  {
    name: 'person/alternateEmail',
    property: 'personAlternateEmail',
    title: 'StudentColumns.personAlternateEmail',
    hidden: true,
    optional: true
  },
  {
    name: 'person/notes',
    property: 'personNotes',
    title: 'StudentColumns.personNotes',
    hidden: true,
    optional: true
  },
  {
    name: 'person/fatherNameGenitive',
    property: 'personFatherNameGenitive',
    title: 'StudentColumns.personFatherNameGenitive',
    hidden: true,
    optional: true
  },
  {
    name: 'person/motherNameGenitive',
    property: 'personMotherNameGenitive',
    title: 'StudentColumns.personMotherNameGenitive',
    hidden: true,
    optional: true
  },
  {
    name: 'person/homeAddress',
    property: 'personHomeAddress',
    title: 'StudentColumns.personHomeAddress',
    hidden: true,
    optional: true
  },
  {
    name: 'person/homePostalCode',
    property: 'personHomePostalCode',
    title: 'StudentColumns.personHomePostalCode',
    hidden: true,
    optional: true
  },
  {
    name: 'person/homeCity',
    property: 'personHomeCity',
    title: 'StudentColumns.personHomeCity',
    hidden: true,
    optional: true
  },
  {
    name: 'person/homeCountry',
    property: 'personHomeCountry',
    title: 'StudentColumns.personHomeCountry',
    hidden: true,
    optional: true
  },
  {
    name: 'person/homePhone',
    property: 'personHomePhone',
    title: 'StudentColumns.personHomePhone',
    hidden: true,
    optional: true
  },
  {
    name: 'person/homeAddressRegion/name',
    property: 'personHomeAddressRegionName',
    title: 'StudentColumns.personHomeAddressRegionName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/temporaryAddress',
    property: 'personTemporaryAddress',
    title: 'StudentColumns.personTemporaryAddress',
    hidden: true,
    optional: true
  },
  {
    name: 'person/temporaryPostalCode',
    property: 'personTemporaryPostalCode',
    title: 'StudentColumns.personTemporaryPostalCode',
    hidden: true,
    optional: true
  },
  {
    name: 'person/temporaryCity',
    property: 'personTemporaryCity',
    title: 'StudentColumns.personTemporaryCity',
    hidden: true,
    optional: true
  },
  {
    name: 'person/temporaryCountry',
    property: 'personTemporaryCountry',
    title: 'StudentColumns.personTemporaryCountry',
    hidden: true,
    optional: true
  },
  {
    name: 'person/temporaryPhone',
    property: 'personTemporaryPhone',
    title: 'StudentColumns.personTemporaryPhone',
    hidden: true,
    optional: true
  },
  {
    name: 'person/temporaryAddressRegion/name',
    property: 'personTemporaryAddressRegionName',
    title: 'StudentColumns.personTemporaryAddressRegionName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/SSN',
    property: 'personSSN',
    title: 'StudentColumns.personSSN',
    hidden: true,
    optional: true
  },
  {
    name: 'person/insuranceProvider/name',
    property: 'personInsuranceProviderName',
    title: 'StudentColumns.personInsuranceProviderName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/insuranceNumber',
    property: 'personInsuranceNumber',
    title: 'StudentColumns.personInsuranceNumber',
    hidden: true,
    optional: true
  },
  {
    name: 'person/mobilePhone',
    property: 'personMobilePhone',
    title: 'StudentColumns.personMobilePhone',
    hidden: true,
    optional: true
  },
  {
    name: 'person/name',
    property: 'personName',
    title: 'StudentColumns.personName',
    hidden: true,
    optional: true
  },
  {
    name: 'studentIdentifier',
    property: 'studentIdentifier',
    title: 'StudentColumns.studentIdentifier',
    hidden: true,
    optional: true
  },
  {
    name: 'studentInstituteIdentifier',
    property: 'studentInstituteIdentifier',
    title: 'StudentColumns.studentInstituteIdentifier',
    hidden: true,
    optional: true
  },
  {
    name: 'department/name',
    property: 'departmentName',
    title: 'StudentColumns.departmentName',
    hidden: true,
    optional: true
  },
  {
    name: 'department/abbreviation',
    property: 'departmentAbbreviation',
    title: 'StudentColumns.departmentAbbreviation',
    hidden: true,
    optional: true
  },
  {
    name: 'department/alternativeCode',
    property: 'departmentAlternativeCode',
    title: 'StudentColumns.departmentAlternativeCode',
    hidden: true,
    optional: true
  },
  {
    name: 'studyProgram/name',
    property: 'studyProgramName',
    title: 'StudentColumns.studyProgramName',
    hidden: true,
    optional: true
  },
  {
    name: 'studyProgram/abbreviation',
    property: 'studyProgramAbbreviation',
    title: 'StudentColumns.studyProgramAbbreviation',
    hidden: true,
    optional: true
  },
  {
    name: 'studyProgram/printName',
    property: 'studyProgramPrintName',
    title: 'StudentColumns.studyProgramPrintName',
    hidden: true,
    optional: true
  },
  {
    name: 'studyProgramSpecialty/name',
    property: 'studyProgramSpecialtyName',
    title: 'StudentColumns.studyProgramSpecialtyName',
    hidden: true,
    optional: true
  },
  {
    name: 'studyProgramSpecialty/abbreviation',
    property: 'studyProgramSpecialtyAbbreviation',
    title: 'StudentColumns.studyProgramSpecialtyAbbreviation',
    hidden: true,
    optional: true
  },
  {
    name: 'studentStatus/alternateName',
    property: 'studentStatusAlternateName',
    title: 'StudentColumns.studentStatusAlternateName',
    formatter: 'TranslationFormatter',
    formatString: 'StudentStatuses.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'schoolGraduated',
    property: 'schoolGraduated',
    title: 'StudentColumns.schoolGraduated',
    hidden: true,
    optional: true
  },
  {
    name: 'schoolGraduatedYear',
    property: 'schoolGraduatedYear',
    title: 'StudentColumns.schoolGraduatedYear',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionNumber',
    property: 'inscriptionNumber',
    title: 'StudentColumns.inscriptionNumber',
    hidden: true,
    optional: true
  },
  {
    name: 'schoolGraduationNumber',
    property: 'schoolGraduationNumber',
    title: 'StudentColumns.schoolGraduationNumber',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionIndex',
    property: 'inscriptionIndex',
    title: 'StudentColumns.inscriptionIndex',
    hidden: true,
    optional: true
  },
  {
    name: 'schoolGraduationGrade',
    property: 'schoolGraduationGrade',
    title: 'StudentColumns.schoolGraduationGrade',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionDate',
    property: 'inscriptionDate',
    title: 'StudentColumns.inscriptionDate',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionSemester/name',
    property: 'inscriptionSemesterName',
    title: 'StudentColumns.inscriptionSemesterName',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionPeriod/alternateName',
    property: 'inscriptionPeriodAlternateName',
    title: 'StudentColumns.inscriptionPeriodAlternateName',
    formatter: 'TranslationFormatter',
    formatString: 'Periods.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionYear/name',
    property: 'inscriptionYearName',
    title: 'StudentColumns.inscriptionYearName',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionMode/abbreviation',
    property: 'inscriptionModeAbbreviation',
    title: 'StudentColumns.inscriptionModeAbbreviation',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionMode/name',
    property: 'inscriptionModeName',
    title: 'StudentColumns.inscriptionModeName',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionDepartment',
    property: 'inscriptionDepartment',
    title: 'StudentColumns.inscriptionDepartment',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionComments',
    property: 'inscriptionComments',
    title: 'StudentColumns.inscriptionComments',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionDecision',
    property: 'inscriptionDecision',
    title: 'StudentColumns.inscriptionDecision',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionRetroYear/name',
    property: 'inscriptionRetroYearName',
    title: 'StudentColumns.inscriptionRetroYearName',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionRetroPeriod/alternateName',
    property: 'inscriptionRetroPeriodAlternateName',
    title: 'StudentColumns.inscriptionRetroPeriodAlternateName',
    formatter: 'TranslationFormatter',
    formatString: 'Periods.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionPoints',
    property: 'inscriptionPoints',
    title: 'StudentColumns.inscriptionPoints',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionModeCategory',
    property: 'inscriptionModeCategory',
    title: 'StudentColumns.inscriptionModeCategory',
    hidden: true,
    optional: true
  },
  {
    name: 'declaredDate',
    property: 'declaredDate',
    title: 'StudentColumns.declaredDate',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationDate',
    property: 'graduationDate',
    title: 'StudentColumns.graduationDate',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationPeriod/alternateName',
    property: 'graduationPeriodAlternateName',
    title: 'StudentColumns.graduationPeriodAlternateName',
    formatter: 'TranslationFormatter',
    formatString: 'Periods.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationYear/name',
    property: 'graduationYearName',
    title: 'StudentColumns.graduationYearName',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationTitle',
    property: 'graduationTitle',
    title: 'StudentColumns.graduationTitle',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationGradeWrittenInWords',
    property: 'graduationGradeWrittenInWords',
    title: 'StudentColumns.graduationGradeWrittenInWords',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationNumber',
    property: 'graduationNumber',
    title: 'StudentColumns.graduationNumber',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationGradeScale/name',
    property: 'graduationGradeScaleName',
    title: 'StudentColumns.graduationGradeScaleName',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationGradeAdjusted',
    property: 'graduationGradeAdjusted',
    title: 'StudentColumns.graduationGradeAdjusted',
    hidden: true,
    optional: true
  },
  {
    name: 'lastObligationDate',
    property: 'lastObligationDate',
    title: 'StudentColumns.lastObligationDate',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationType/name',
    property: 'graduationTypeName',
    title: 'StudentColumns.graduationTypeName',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationRankByInscriptionYear',
    property: 'graduationRankByInscriptionYear',
    title: 'StudentColumns.graduationRankByInscriptionYear',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationRankByGraduationYear',
    property: 'graduationRankByGraduationYear',
    title: 'StudentColumns.graduationRankByGraduationYear',
    hidden: true,
    optional: true
  },
  {
    name: 'removalDate',
    property: 'removalDate',
    title: 'StudentColumns.removalDate',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'removalRequest',
    property: 'removalRequest',
    title: 'StudentColumns.removalRequest',
    hidden: true,
    optional: true
  },
  {
    name: 'removalReason',
    property: 'removalReason',
    title: 'StudentColumns.removalReason',
    hidden: true,
    optional: true
  },
  {
    name: 'removalDecision',
    property: 'removalDecision',
    title: 'StudentColumns.removalDecision',
    hidden: true,
    optional: true
  },
  {
    name: 'removalDepartment',
    property: 'removalDepartment',
    title: 'StudentColumns.removalDepartment',
    hidden: true,
    optional: true
  },
  {
    name: 'removalNumber',
    property: 'removalNumber',
    title: 'StudentColumns.removalNumber',
    hidden: true,
    optional: true
  },
  {
    name: 'removalComments',
    property: 'removalComments',
    title: 'StudentColumns.removalComments',
    hidden: true,
    optional: true
  },
  {
    name: 'comments',
    property: 'comments',
    title: 'StudentColumns.comments',
    hidden: true,
    optional: true
  },
  {
    name: 'militaryStatus/name',
    property: 'militaryStatusName',
    title: 'StudentColumns.militaryStatusName',
    hidden: true,
    optional: true
  },
  {
    name: 'category/name',
    property: 'categoryName',
    title: 'StudentColumns.categoryName',
    hidden: true,
    optional: true
  },
  {
    name: 'uniqueIdentifier',
    property: 'uniqueIdentifier',
    title: 'StudentColumns.uniqueIdentifier',
    hidden: true,
    optional: true
  },
  {
    name: 'dateModified',
    property: 'dateModified',
    title: 'StudentColumns.dateModified',
    formatter: 'DateTimeFormatter',
    formatString: 'shortDate',
    hidden: true,
    optional: true
  },
  {
    name: 'rate',
    property: 'rate',
    title: 'StudentColumns.rate',
    hidden: true,
    optional: true
  },
  {
    name: 'user/alternateName',
    property: 'userAlternateName',
    title: 'StudentColumns.userAlternateName',
    hidden: true,
    optional: true
  },
  {
    name: 'user/name',
    property: 'userName',
    title: 'StudentColumns.userName',
    hidden: true,
    optional: true
  },
  {
    name: 'specialty',
    property: 'specialty',
    title: 'StudentColumns.specialty',
    hidden: true,
    optional: true
  },
  {
    name: 'customField1',
    property: 'customField1',
    title: 'StudentColumns.customField1',
    hidden: true,
    optional: true
  },
  {
    name: 'customField2',
    property: 'customField2',
    title: 'StudentColumns.customField2',
    hidden: true,
    optional: true
  },
  {
    name: 'customField3',
    property: 'customField3',
    title: 'StudentColumns.customField3',
    hidden: true,
    optional: true
  },
  {
    name: 'customField4',
    property: 'customField4',
    title: 'StudentColumns.customField4',
    hidden: true,
    optional: true
  },
  {
    name: 'customField5',
    property: 'customField5',
    title: 'StudentColumns.customField5',
    hidden: true,
    optional: true
  }
];

