import { TableColumnConfiguration } from '@universis/ngx-tables';

export const STUDENT_SHARED_COLUMNS: TableColumnConfiguration[] = [
  {
    name: 'person/familyName',
    property: 'studentFamilyName',
    title: 'Students.Shared.FamilyName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/givenName',
    property: 'studentGivenName',
    title: 'Students.Shared.GivenName',
    hidden: true,
    optional: true
  },
  {
    name: 'person/email',
    property: 'studentEmail',
    title: 'Students.Shared.Email',
    hidden: true,
    optional: true
  },
  {
    name: 'person/SSN',
    property: 'studentSSN',
    title: 'Students.Shared.SSN',
    hidden: true,
    optional: true
  },
  {
    name: 'person/mobilePhone',
    property: 'studentMobilePhone',
    title: 'Students.Shared.MobilePhone',
    hidden: true,
    optional: true
  },
  {
    name: 'person/name',
    property: 'studentName',
    title: 'Students.Shared.FullName',
    hidden: true,
    optional: true
  },
  {
    name: 'semester',
    property: 'studentSemester',
    title: 'Students.Shared.Semester',
    hidden: true,
    optional: true
  },
  {
    name: 'studentIdentifier',
    property: 'studentIdentifier',
    title: 'Students.Shared.StudentIdentifier',
    hidden: true,
    optional: true
  },
  {
    name: 'department/name',
    property: 'studentDepartmentName',
    title: 'Students.Shared.DepartmentName',
    hidden: true,
    optional: true
  },
  {
    name: 'department/abbreviation',
    property: 'studentDepartmentAbbreviation',
    title: 'Students.Shared.DepartmentAbbreviation',
    hidden: true,
    optional: true
  },
  {
    name: 'studyProgram/name',
    property: 'studentStudyProgramName',
    title: 'Students.Shared.StudyProgramName',
    hidden: true,
    optional: true
  },
  {
    name: 'studyProgram/abbreviation',
    property: 'studentStudyProgramAbbreviation',
    title: 'Students.Shared.StudyProgramAbbreviation',
    hidden: true,
    optional: true
  },
  {
    name: 'studyProgramSpecialty/name',
    property: 'studentStudyProgramSpecialtyName',
    title: 'Students.Shared.StudyProgramSpecialtyName',
    hidden: true,
    optional: true
  },
  {
    name: 'studyProgramSpecialty/abbreviation',
    property: 'studentStudyProgramSpecialtyAbbreviation',
    title: 'Students.Shared.StudyProgramSpecialtyAbbreviation',
    hidden: true,
    optional: true
  },
  {
    name: 'studentStatus/alternateName',
    property: 'studentStatusAlternateName',
    title: 'Students.Shared.Status',
    formatter: 'TranslationFormatter',
    formatString: 'StudentStatuses.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'uniqueIdentifier',
    property: 'studentUniqueIdentifier',
    title: 'Students.Shared.UniqueIdentifier',
    hidden: true,
    optional: true
  },
  {
    name: 'user/name',
    property: 'studentUserName',
    title: 'Students.Shared.UserName',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionYear/alternateName',
    property: 'studentInscriptionYear',
    title: 'Students.Shared.InscriptionYear',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionMode/name',
    property: 'studentInscriptionMode',
    title: 'Students.Shared.InscriptionMode',
    hidden: true,
    optional: true
  },
  {
    name: 'inscriptionPeriod/alternateName',
    property: 'studentInscriptionPeriod',
    title: 'Students.Shared.InscriptionPeriod',
    formatter: 'TranslationFormatter',
    formatString: 'Periods.${value}',
    hidden: true,
    optional: true
  },
  {
    name: 'graduationYear/alternateName',
    property: 'studentGraduationYear',
    title: 'Students.Shared.GraduationYear',
    hidden: true,
    optional: true,
  }
];
