import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationsPreviewLatestHistoryComponent } from './registrations-preview-latest-history.component';

describe('RegistrationsPreviewLatestHistoryComponent', () => {
  let component: RegistrationsPreviewLatestHistoryComponent;
  let fixture: ComponentFixture<RegistrationsPreviewLatestHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationsPreviewLatestHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationsPreviewLatestHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
