import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActiveDepartmentService} from "../../../registrar-shared/services/activeDepartmentService.service";
import { ConfigurationService } from '@universis/common';
import { ApplicationSettings } from '../../../registrar-shared/registrar-shared.module';

@Component({
  selector: 'app-dashboard-pending-student-requests',
  templateUrl: './dashboard-pending-student-requests.component.html',
  styleUrls: ['./dashboard-pending-student-requests.component.scss']
})
export class DashboardPendingStudentRequestsComponent implements OnInit {

  public lastRequests: any;

  constructor(private _context: AngularDataContext,
              private _activeDepartmentService: ActiveDepartmentService,
              private _configurationService: ConfigurationService ) { }

  async ngOnInit() {
    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    const excludeRequestTypes =  (<ApplicationSettings>this._configurationService.settings.app).excludeRequestTypes;


    const query = this._context.model('StudentRequestActions')
        .where('actionStatus/alternateName ').equal('ActiveActionStatus')
        .and('student/department').equal(activeDepartment.id);

      if(Array.isArray(excludeRequestTypes) && excludeRequestTypes.length>0 && excludeRequestTypes.every(val => typeof val === 'string')){
        for (let i = 0; i < excludeRequestTypes.length; i++) {
          query.and('additionalType').notEqual(excludeRequestTypes[i]);
        }
      }
    this.lastRequests = await query.expand('student($expand=person($select=id, givenName, familyName))')
        .orderByDescending('dateModified')
        .take(3)
        .getList();
  }

}
