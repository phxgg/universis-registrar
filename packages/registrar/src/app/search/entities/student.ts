export interface Student {
    familyName: string;
    givenName: string;
    studentIdentifier: number;
    id: number;
    departmentId: number;
    departmentName: string;
    label: string;
    type: string
  }
