import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { RouterModalYesNo } from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
    selector: 'app-yes-no',
    template: `
        <p>
            Are you sure to complete this action?
        </p>
    `,
    styles: [`
    `]
})
export class YesNoComponent extends RouterModalYesNo implements OnInit, OnDestroy {
    @Input() modalTitle = 'A Yes/No Message';
    private subscription: Subscription;

    constructor(protected router: Router, protected activatedRoute: ActivatedRoute) {
        // call super constructor
        super(router, activatedRoute);
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    ngOnInit(): void {
        // get yes button text from route data parameters
        this.subscription = this.activatedRoute.data.subscribe( data => {
            this.yesButtonText = data.yesButtonText;
        });
        setTimeout(() => {
            this.yesButtonDisabled = true;
        }, 5000);
    }
    //

    async no() {
        // close
        return this.close();
    }

    async yes() {
        // do something and close
        return this.close();
    }

}
